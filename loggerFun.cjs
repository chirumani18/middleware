const fs=require("fs")
function logger(request, response, next) {
    let data = `The request method is ${request.method} and the url is ${request.url} \n`
    if (request.url !== '/logs') {
        fs.appendFile('data/logs.log', data, (err) => {
            if (err) {
                console.log('error while writing')
            }
            else {
                next()
            }
        })
    }
    else {
        next()
    }
}

module.exports = logger